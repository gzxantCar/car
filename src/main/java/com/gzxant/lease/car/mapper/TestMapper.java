package com.gzxant.lease.car.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;

import com.gzxant.lease.car.model.Test;

public interface TestMapper {

	@Insert("INSERT INTO test(test_a, test_b) VALUES(#{testA}, #{testB})")
	int createTest(Test test);
	
	List<Test> getTests();

}
