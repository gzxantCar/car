package com.gzxant.lease.car.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gzxant.lease.car.model.Test;
import com.gzxant.lease.car.service.TestService;

@Controller
@RequestMapping("test")
public class TestController {

	@Autowired
	TestService testService;
	
	@RequestMapping("aaa")
	@ResponseBody
	public List<Test> aaa() {
		return testService.testAaa();
	}
	
}
