package com.gzxant.lease.car.model;

public class Test {
	private Long id;
	private Integer testA;
	private String testB;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getTestA() {
		return testA;
	}
	public void setTestA(Integer testA) {
		this.testA = testA;
	}
	public String getTestB() {
		return testB;
	}
	public void setTestB(String testB) {
		this.testB = testB;
	}
}
