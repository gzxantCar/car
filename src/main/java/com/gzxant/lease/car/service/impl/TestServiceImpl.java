package com.gzxant.lease.car.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gzxant.lease.car.mapper.TestMapper;
import com.gzxant.lease.car.model.Test;
import com.gzxant.lease.car.service.TestService;

@Service("testService")
public class TestServiceImpl implements TestService {
	
	private static final Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);
	
	@Autowired
	TestMapper testMapper;

	@Override
	public List<Test> testAaa() {
		logger.info("test ------ ");
		Test test = new Test();
		test.setTestA(1);
		test.setTestB(">>>");
		int count = testMapper.createTest(test);
		logger.info("test:{}", count);
		List<Test> tests = testMapper.getTests();
		return tests;
	}

}
